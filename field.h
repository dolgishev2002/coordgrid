#ifndef FIELD_H
#define FIELD_H

#include <QGraphicsView>
#include <QGraphicsItemGroup>
#include <QResizeEvent>
#include <QVector>
#include <cmath>
#include <QDebug>
#include <QFileDialog>
#include <QMessageBox>
#include <QApplication>

class Field : public QGraphicsView
{
    Q_OBJECT
public:
    Field(QWidget *parent = 0);
    ~Field();
    QGraphicsItem *getLastDot();

    void addDot(int x, int y);
    void setSizeFactor(double sizeFactor);
    void setGridColor(QColor col);
    void setGridMainColor(QColor col);
    void setDotColor(QColor col);
    void setDotLineColor(QColor col);
    void clearDots();
    void showLines();
    void hideLines();
    void showCross(int x, int y);
    void hideCross();
    void redrawCross(int x, int y);
    int dotCount();
    void setShift(int x, int y);
    void undo();
    void save();
    void load();
    void exit();
private:
    void loadFromFile();
    void saveToFile();
    QString filename;
    bool showingCross = 0;
    bool showingLines = 0;
    QSize *sz;
    double sizeFactor = 20;
    int xshift = 0, yshift = 0;
    void resizeEvent(QResizeEvent* event);
    void redrawGrid();
    void redrawDots();
    QGraphicsScene *scene;
    QGraphicsItemGroup *group_hline;
    QGraphicsItemGroup *group_vline;
    QGraphicsItemGroup *group_dot;
    QGraphicsItemGroup *group_dot_tmp;
    QGraphicsItemGroup *group_dlines;
    QGraphicsItemGroup *group_cross;
    QPen gridPen;
    QPen gridFatPen;
    QPen dotPen;
    QPen dotLinePen;
    QBrush dotBrush;
};

#endif // FIELD_H
