#include "mainwindow.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QGraphicsView"
#include "QMessageBox"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowTitle("Координатная Плоскость");
    ui->graphicsView->setShift(xshift,yshift);
}

MainWindow::~MainWindow()
{
    delete ui;
}

// // // // // // // // // // // // //
// //                            // //
// //  UI - SLIDERS & BUTTONS    // //
// //                            // //
// // // // // // // // // // // // //

void MainWindow::on_horizontalSlider_valueChanged(int value)
{
    this->x = ui->horizontalSlider->value()*sizeFactor;
    this->updateCoord(ui->horizontalSlider->value(),ui->verticalSlider->value());
}

void MainWindow::on_verticalSlider_valueChanged(int value)
{
    this->y = ui->verticalSlider->value()*sizeFactor;
    this->updateCoord(ui->horizontalSlider->value(),ui->verticalSlider->value());
}

void MainWindow::on_xSpin_valueChanged(int arg1) // horiz (X) textbox
{
    this->x = ui->xSpin->value()*sizeFactor;
    this->updateCoord(ui->xSpin->value(),ui->ySpin->value());
}

void MainWindow::on_ySpin_valueChanged(int arg1) // vertic (Y) textbox
{
    this->y = ui->ySpin->value()*sizeFactor;
    this->updateCoord(ui->xSpin->value(),ui->ySpin->value());
}


void MainWindow::on_addDotBtn_clicked()
{
    ui->graphicsView->addDot(round((x)/sizeFactor)*sizeFactor,round((gridSize.height()*sizeFactor - y)/sizeFactor)*sizeFactor);
    this->updateLastCoord(x/sizeFactor,y/sizeFactor);
}



void MainWindow::updateCoord(int x, int y)
{
    ui->horizontalSlider->setValue(x);
    ui->verticalSlider->setValue(y);

    ui->xSpin->setValue(x);
    ui->ySpin->setValue(y);

    ui->graphicsView->redrawCross(x, gridSize.height() - y);
}

void MainWindow::updateLastCoord(int x, int y)
{
    ui->CursorPos->setText("Последняя точка: X=" + QString::number(x) + ", Y=" + QString::number(y));
}

// // // // // // // // // // // // //
// //                            // //
// //     UI - MOUSE EVENTS      // //
// //                            // //
// // // // // // // // // // // // //


void MainWindow::mousePressEvent(QMouseEvent *e)
{
    int offx = ui->graphicsView->pos().x(); // offset from top left corner
    int offy = ui->graphicsView->pos().y(); //
    QPointF lpos = e->localPos();
    if(e->button() == Qt::LeftButton && lpos.x() > offx && lpos.x() <= gridSize.height()*sizeFactor + offx && lpos.y() > offy && lpos.y() <= gridSize.width()*sizeFactor + offy) //if clicked on field obj
    {
        this->x = round((lpos.x())/sizeFactor-offx/sizeFactor)*sizeFactor;
        this->y = gridSize.height() -  round((lpos.y()-offy)/sizeFactor)*sizeFactor;
        ui->graphicsView->addDot(round((lpos.x()-offx)/sizeFactor)*sizeFactor,round((lpos.y()-offy)/sizeFactor)*sizeFactor);

        this->updateCoord(round((lpos.x()-offx)/sizeFactor),  gridSize.height() - round((lpos.y()-offy)/sizeFactor));
        this->updateLastCoord(round((lpos.x()-offx)/sizeFactor),  gridSize.height() - round((lpos.y()-offy)/sizeFactor));
        ui->graphicsView->redrawCross(x/sizeFactor, gridSize.height() - y/sizeFactor);
    }
}

// // // // // // // // // // // // //
// //                            // //
// //        UI - COLORS         // //
// //                            // //
// // // // // // // // // // // // //

QColor MainWindow::colorPick()
{
    QColorDialog pick;
    return pick.getColor();
}

void MainWindow::on_dotColorBtn_clicked()
{
    ui->graphicsView->setDotColor(this->colorPick());
}


void MainWindow::on_gridColorBtn_clicked()
{
    ui->graphicsView->setGridColor(this->colorPick());
}

void MainWindow::on_dotLineColorBtn_clicked()
{
    ui->graphicsView->setDotLineColor(this->colorPick());
}

void MainWindow::on_gridMainColrBtn_clicked()
{
    ui->graphicsView->setGridMainColor(this->colorPick());
}

// // // // // // // // // // // // //
// //                            // //
// //     UI - CLEAR & UNDO      // //
// //                            // //
// // // // // // // // // // // // //

void MainWindow::on_clearButton_clicked()
{
    if(QMessageBox::Yes == QMessageBox(QMessageBox::Information, "Внимание!!!", "Вы хотите очистить поле?", QMessageBox::No|QMessageBox::Yes).exec())
    {
        ui->graphicsView->clearDots();
        ui->CursorPos->setText("Точки не заданы");
    }
}

void MainWindow::on_clearButton_2_clicked() //undo
{
    int offx = ui->graphicsView->pos().x(); // offset from top left corner
    int offy = ui->graphicsView->pos().y(); //
    if(ui->graphicsView->dotCount() >= 1)
    {
        ui->graphicsView->undo();
        if(ui->graphicsView->dotCount() >= 1)
        {
            QPointF pos = QPointF((ui->graphicsView->getLastDot()->pos().x())/sizeFactor,(ui->graphicsView->getLastDot()->pos().y())/sizeFactor);
            qDebug() << pos;
            this->updateCoord(pos.x(),gridSize.height() - pos.y());
        }
        else
        {
            ui->CursorPos->setText("Точки не заданы");
        }
    }

}

void MainWindow::on_checkBox_stateChanged(int arg1)
{
    if(arg1 == Qt::CheckState::Checked)
    {
        ui->graphicsView->showLines();
    }
    if(arg1 == Qt::CheckState::Unchecked)
    {
        ui->graphicsView->hideLines();
    }
}

void MainWindow::on_aim_Checkbox_stateChanged(int arg1)
{
    if(arg1 == Qt::CheckState::Checked)
    {
        ui->graphicsView->showCross(x/sizeFactor, gridSize.height() - y/sizeFactor);
    }
    if(arg1 == Qt::CheckState::Unchecked)
    {
        ui->graphicsView->hideCross();
    }
}

void MainWindow::on_openBtn_clicked()
{
    ui->graphicsView->load();
}

void MainWindow::on_saveBtn_clicked()
{
    ui->graphicsView->save();
}

void MainWindow::on_exitBtn_clicked()
{
    ui->graphicsView->exit();
}
