#include "field.h"

Field::Field(QWidget *parent) : QGraphicsView(parent)
{
    this->sz = new QSize(500,500);
    this->setMinimumHeight(this->sz->height());
    this->setMinimumWidth(this->sz->width());

    this->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    this->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    gridPen = QPen(Qt::gray);
    gridFatPen = gridPen;
    gridFatPen.setColor(Qt::black);
    gridFatPen.setWidth(gridPen.width()+3);
    dotPen = QPen(Qt::darkRed);
    dotBrush = QBrush(Qt::darkRed);
    dotLinePen.setWidth(dotPen.width()+1);
    dotLinePen.setColor(Qt::red);

    scene = new QGraphicsScene();
    group_hline = new QGraphicsItemGroup();
    group_vline = new QGraphicsItemGroup();
    group_dot = new QGraphicsItemGroup();
    group_dlines = new QGraphicsItemGroup();
    group_cross = new QGraphicsItemGroup();
    scene->addItem(group_hline);
    scene->addItem(group_vline);
    scene->addItem(group_dot);
    scene->addItem(group_dlines);
    scene->addItem(group_cross);
    group_dot->setZValue(10);

    this->setScene(scene);
    redrawGrid();
}

Field::~Field()
{

}

void Field::resizeEvent(QResizeEvent* event)
{
    this->sz->rheight() = event->size().height();
    this->sz->rwidth() = event->size().width();
    QGraphicsView::resizeEvent(event);
    scene->setSceneRect(0,0,this->sz->height(),this->sz->width());
    this->setMinimumHeight(this->sz->height());
    this->setMinimumWidth(this->sz->width());

}

void Field::redrawGrid()
{
    for(auto i : group_hline->childItems())
    {
        scene->removeItem(i);
    }

    for(auto i : group_vline->childItems())
    {
        scene->removeItem(i);
    }

    int h = this->sz->height();
    int w = this->sz->width();
    for(int i = 0; i <= 25; i++)
    {
        if(i == xshift)
        {
            group_hline->addToGroup(scene->addLine(i*sizeFactor, 0, i*sizeFactor, 40*sizeFactor, gridFatPen));
            group_hline->childItems().last()->setZValue(2);
        }
        else
            group_hline->addToGroup(scene->addLine(i*sizeFactor, 0, i*sizeFactor, 40*sizeFactor, gridPen));
    }

    for(int j = 0; j <= 25; j++)
    {
        if(25 - j == yshift)
        {
            group_vline->addToGroup(scene->addLine(0, j*sizeFactor, 40*sizeFactor, j*sizeFactor, gridFatPen));
            group_vline->childItems().last()->setZValue(2);
        }
        else
            group_vline->addToGroup(scene->addLine(0, j*sizeFactor, 40*sizeFactor, j*sizeFactor, gridPen));
    }
}

void Field::redrawDots()
{
    group_dot_tmp = new QGraphicsItemGroup();
    scene->addItem(group_dot_tmp);
    for(auto i : group_dot->childItems())
    {
        group_dot_tmp->addToGroup(scene->addEllipse(-5, -5, 10, 10, dotPen, dotBrush));
        group_dot_tmp->childItems().last()->setPos(i->pos());
        scene->removeItem(i);
    }
    scene->addItem(group_dot);
    for(auto i : group_dot_tmp->childItems())
    {
        group_dot->addToGroup(scene->addEllipse(-5, -5, 10, 10, dotPen, dotBrush));
        group_dot->childItems().last()->setPos(i->pos());
        scene->removeItem(i);
    }
}

void Field::addDot(int x, int y)
{
    bool make = true;
    for(auto i : group_dot->childItems())
    {
        if(i == scene->itemAt(x,y,this->transform())) make = false;
    }
    if(make)
    {
        group_dot->addToGroup(scene->addEllipse(-5, -5, 10, 10, dotPen, dotBrush));
        group_dot->childItems().last()->setPos(x,y);
        if(showingLines && group_dot->childItems().size() > 1)
        {
            if(group_dot->childItems().size() > 3) scene->removeItem(group_dlines->childItems().last());
            group_dlines->addToGroup(scene->addLine(group_dot->childItems().at(group_dot->childItems().size() - 2) ->x(),group_dot->childItems().at(group_dot->childItems().size() - 2)->y(),x,y, dotLinePen));
            if(group_dot->childItems().size() > 2) group_dlines->addToGroup(scene->addLine(group_dot->childItems().at(0) ->x(),
                                                    group_dot->childItems().at(0)->y(),
                                                    group_dot->childItems().at(group_dot->childItems().size()-1)->x(),
                                                    (group_dot->childItems().at(group_dot->childItems().size()-1)->y()), dotLinePen));
        }
    }
    else
    {
        qDebug() << "Dot at: x=" << x << " y=" << y << "already exist!";
    }
}

void Field::setSizeFactor(double sizeFactor)
{
    this->sizeFactor = sizeFactor;
}

void Field::setGridColor(QColor col)
{
    gridPen.setColor(col);
    redrawGrid();
}

void Field::setGridMainColor(QColor col)
{
    gridFatPen.setColor(col);
    redrawGrid();
}

void Field::setDotColor(QColor col)
{
    dotBrush.setColor(col);
    dotPen.setColor(col);

    redrawDots();
}

void Field::setDotLineColor(QColor col)
{
    dotLinePen.setColor(col);
    if(showingLines)
    {
        hideLines();
        showLines();
    }
}

void Field::clearDots()
{
    for(auto i : group_dot->childItems())
    {
        scene->removeItem(i);
    }
    if(showingLines)
    {
        hideLines();
        showLines();
    }
}

void Field::showLines()
{
    showingLines = true;
    for(int i = 1; i < group_dot->childItems().size(); i++)
    {
        group_dlines->addToGroup(scene->addLine(group_dot->childItems().at(i-1) ->x(),group_dot->childItems().at(i-1)->y(),group_dot->childItems().at(i)->x(),(group_dot->childItems().at(i)->y()), dotLinePen));
    }
    if(group_dot->childItems().size() > 2) group_dlines->addToGroup(scene->addLine(group_dot->childItems().at(0) ->x(),
                                            group_dot->childItems().at(0)->y(),
                                            group_dot->childItems().at(group_dot->childItems().size()-1)->x(),
                                            (group_dot->childItems().at(group_dot->childItems().size()-1)->y()), dotLinePen));
}

void Field::hideLines()
{
    showingLines = false;
    for(auto i : group_dlines->childItems())
    {
        scene->removeItem(i);
    }
}

void Field::showCross(int x, int y)
{
    showingCross = 1;
    redrawCross(x, y);
}

void Field::hideCross()
{
    showingCross = 0;
    redrawCross(0, 0);
}

void Field::redrawCross(int x, int y)
{
    if(group_cross->childItems().size() >= 1)
    {
        for(auto i : group_cross->childItems())
        {
            scene->removeItem(i);
        }
    }
    if(showingCross)
    {
        group_cross->addToGroup(scene->addLine(x*sizeFactor, 0, x*sizeFactor, 40*sizeFactor, gridFatPen));
        group_cross->addToGroup(scene->addLine(0,y*sizeFactor, 40*sizeFactor, y*sizeFactor, gridFatPen));
    }
}

int Field::dotCount()
{
    return group_dot->childItems().size();
}

QGraphicsItem* Field::getLastDot()
{
    return group_dot->childItems().last();
}

void Field::setShift(int x, int y)
{
    xshift = x;
    yshift = y;
}

void Field::undo()
{
    scene->removeItem(group_dot->childItems().last());
    if(showingLines)
    {
        hideLines();
        showLines();
    }
}

void Field::load()
{
    QString fileName = QFileDialog::getOpenFileName(this,
        tr("Открыть чертеж"), QDir::homePath(), tr("Файл Чертежа (*.cbf)"));
    if(fileName == "")
    {
        QMessageBox::warning(this, tr("Внимание!"),
                                       tr("Файл не выбран!"),
                                       QMessageBox::Ok);
    }
    else
    {
        this->clearDots();
        filename = fileName;
        loadFromFile();
    }
}

void Field::exit()
{
    if(this->dotCount() >= 1)
    {
        int ask = QMessageBox::warning(this, tr("Внимание!"),
                                               tr("Изменения не сохранены.\nСохранить?"),
                                               QMessageBox::Yes | QMessageBox::No);
        if(ask == QMessageBox::No)
        {
            QApplication::quit();
        }
        if(ask == QMessageBox::Yes)
        {
            this->save();
        }
    }
    else
    {
        QApplication::quit();
    }
}

void Field::loadFromFile()
{

    QFile file(filename);
    qDebug() << file;
    if ((file.exists())&&(file.open(QIODevice::ReadOnly)))
    {
        qDebug() << "Works";
        QTextStream fin(&file);
        int tmp;
        fin >> tmp;
        //sz->setHeight(tmp);
        fin >> tmp;
        //sz->setWidth(tmp);

        fin >> tmp; //n of dots
        qDebug() << tmp;
        for(int i = 0; i < tmp; i++)
        {
            int x, y;
            fin >> x >> y;
            qDebug() << x << " " << y;
            addDot(x*sizeFactor, this->sz->height() - y*sizeFactor);
        }
    }
    file.close();
}

void Field::saveToFile()
{

    QFile file(filename);
    if (file.open(QIODevice::WriteOnly))
    {
        QTextStream fout(&file);
        fout << sz->height() << " " << sz->width() << endl;
        fout << dotCount() << endl;
        for(auto i : group_dot->childItems())
        {
            fout << (i->pos().rx())/sizeFactor << " " << round(1.0*(sz->height() - i->pos().ry())/sizeFactor) << endl;
        }
    }
    file.close();
}

void Field::save()
{
    QString fileName = QFileDialog::getSaveFileName(this,
        tr("Сохранить чертеж"), QDir::homePath(), tr("Файл Чертежа (*.cbf)"));
    if(fileName == "")
    {
        QMessageBox::warning(this, tr("Внимание!"),
                                       tr("Файл не выбран!"),
                                       QMessageBox::Ok);
    }
    else
    {
        filename = fileName;
        if(filename.right(4) != ".cbf")
        {
            filename += ".cbf";
        }
        saveToFile();
    }
}
